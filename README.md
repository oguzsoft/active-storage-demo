## Active Storage Demo App in Rails 6

##### Create a new rails app with using postgresql

```bash
rails new action-text-demo -d postgresql
cd action-text-demo
bundle install
```

##### Scaffolding

```bash
rails g scaffold Person name
```

##### Create database and migrating

```bash
rails db:create
rails db:migrate
```

##### Install Active Storage

```bash
rails action_text:install
```

##### Update ```config/routes.rb```

```ruby
root 'people#index'
```

* save and exit

##### Update Person model in ```model/person.rb```

```ruby
class Person < ApplicationRecord
    has_one_attached :file
end
```

##### Update Person form in ```app/views/people/_form.html.erb```

```ruby
  <div class="field">
    <%= form.label :file %>
    <%= form.file_field :file %>
  </div>
```

#### Finally 

Add file param in ```people_controller.rb```

* person params should look like this

```ruby
def person_params
    params.require(:person).permit(:name, :file)
end
```

* After updated files, run ```rails db:migrate``` in console

##### Show or download uploaded file in

* Add those lines in ```app/views/people/show.html.erb```
* person show file should look like this

```ruby
<p>
  <strong>File:</strong>
  <%= link_to 'View File', @person.file %>
  <%= link_to 'Download File', @person.file, download: '' %>
</p>
```

##### Start Rails Server 

```bash
rails s
```
